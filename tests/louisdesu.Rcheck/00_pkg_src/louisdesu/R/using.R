#' Install and load packages
#'
#' This function installs and loads the specified packages. It checks if the packages are already installed, and if not, it installs them from source and then loads them.
#'
#' @param ... Character vectors specifying the packages to install and load.
#' @param load Logical indicating whether to load the packages after installation (default: TRUE).
#' @param lips Logical indicating whether to display installation messages (default: FALSE).
#' @param junk Logical indicating whether to install package dependencies (default: TRUE).
#' @importFrom pacman p_load
#' @importFrom utils install.packages
#' @examples
#' \dontrun{
#' using("tidyr", load = FALSE)
#' }
#' @export
using <- function(..., load = TRUE, lips = FALSE, junk = TRUE) {
  libs <- c(...)
  installed <- pacman::p_loaded(libs, character.only = TRUE)
  need <- libs[!installed]
  
  # Install missing packages
  if (length(need) > 0) {
    print(paste("Installing:", paste(need, collapse = ", ")))
    utils::install.packages(need, type = "source", quiet = lips, dependencies = junk)
    installed <- pacman::p_loaded(libs, character.only = TRUE)
  }
  
  # Load nominated packages
  if (load) {
    print(paste("Loading:", paste(libs[installed], collapse = ", ")))
    pacman::p_load(libs[installed], character.only = TRUE)
  }
}
