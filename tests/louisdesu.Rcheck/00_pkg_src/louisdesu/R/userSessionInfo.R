#' Print user-defined functions
#'
#' This function prints the names of user-defined functions in the global environment.
#'
#' @param ... Additional arguments (not used).
#' @examples
#' userSessionInfo()
#'
#' @export
userSessionInfo <- function(...) {
  cat("User-defined functions:\n")
  func_names <- ls(envir = .GlobalEnv, all.names = FALSE, pattern = "^\\w+\\$")
  if (length(func_names) > 0) {
    cat(paste0("- ", func_names, "\n"))
  } else {
    cat("No user-defined functions found.\n")
  }
}
