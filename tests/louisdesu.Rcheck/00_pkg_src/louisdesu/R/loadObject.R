#' Load and retrieve an object from a file
#'
#' This function loads an R object from a file and retrieves it.
#'
#' @param file The file path to load the object from.
#' @param object The name of the object to retrieve.
#' @return The retrieved object.
#' @examples
#' file <- system.file("extdata", "example.rda", package = "yourPackageName")
#' if (file.exists(file)) {
#'     my_object <- loadObject(file, "my_data")
#' }
#' @export
loadObject <- function(file, object) {
  if (!file.exists(file)) {
    stop(paste("File", file, "does not exist."))
  }
  
  E <- new.env()
  load(file = file, envir = E)
  
  if (!exists(object, envir = E)) {
    stop(paste("Object", object, "does not exist in the file."))
  }
  
  get(object, envir = E, inherits = FALSE)
}
