# README

# louisdesu

This package contains multiple utility functions that you can use to augment your R workflow. Here, we have provided detailed documentation about how to install this package and use its functions.

## Installation

To install this package from GitLab, you need to have the `devtools` package installed in your R environment. You can install **louisdesu** using the following code:

```r
# Install devtools if not already installed
if (!require(devtools)) {
  install.packages("devtools")
}

# Install louisdesu from GitLab
devtools::install_git("https://gitlab.com/louisdesu/louisdesu.git")
```

## Functions

Here is a brief description of the functions available in this package:

- `using()`: Install and load R packages easily. This function checks if the packages are already installed, if not, it installs them from source and then loads them so you can effectively do `install.packages()` and `library()` in a single step.

- `userSessionInfo()`: Print the names of user-defined functions in the global environment for reproducibility purposes.

- `theme_UNSW()`: Create a custom theme that is in-line with University of New South Wales (UNSW) publications styleguide (requires `Sommet` font family).

- `%S>%`: Save the output of the preceding expression within a piping workflow to a CSV file named "temp.csv" without printing any messages.

- `regroup()`: Regroup your data without having to call `ungroup()` then `group_by()` each time.

- `quietly()`: Execute code while controlling the suppression of messages, warnings, and output. This is particularly useful when you want to allow some messages to be preserved in for an R Notebook but want to suppress more verbose and unsightly warnings.

- `%0>%`: Suppress warnings and messages while applying a function to an argument within a piping workflow.

- `printGap()`: Print the elements of a vector with a gap between each element – particularly useful when interacting with the console for logging purposes.

- `%->%`: Assign the output of the preceding expression within a piping workflow to a variable with the specified name.

- `loadObject()`: Load an R object from a container file and load it into the Global Environment without having to load the entire container file – particularly useful when working with very large files etc.

- `drop_all()`: Remove columns from a data frame that contain all missing values.

- `convert_na()`: Convert empty strings in a data frame to NA prior to missingness analysis.

- `%P>%`: Print the current object within a piping workflow to the console.

For detailed usage and examples of these functions, please refer to the function documentation in R using `?function_name`.

## Example Usage

Here is an example of how you might use some of the functions in this package:

```r
# Load the library
library(louisdesu)

# Install and load required packages
using("ggplot2", "dplyr")

# Load data
data(mtcars)

# Regroup data
mtcars_grouped <- group_by(cyl) %>%
  regroup(mtcars, cyl)

# Create a ggplot with custom theme
ggplot(mtcars, aes(x = mpg, y = cyl)) + 
  geom_point() + 
  theme_UNSW()

# Print user-defined functions
userSessionInfo()
```

## Getting Help

If you encounter a bug or have any specific question about this package, please open an issue on the [GitLab repository](https://gitlab.com/louisdesu/louisdesu.git).

Happy coding!