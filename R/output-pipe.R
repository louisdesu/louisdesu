#' Assign output within a piping workflow
#'
#' This function assigns the output of the preceding expression within a piping workflow to a variable with the specified name.
#'
#' @name %->%
#' @param value The value to assign.
#' @param df A lazy expression containing the variable name and environment.
#' @return The assigned value.
#' @importFrom rlang enquo
#' @examples
#' require(magrittr)
#' start <- rnorm(100)
#' finish <- function(x) mean(x)
#' start %->% temp %>% finish
#' @export
`%->%` <- function(value, df) {
  df <- rlang::enquo(df)
  suppressWarnings(
    suppressMessages(
      invisible(
        assign(x = rlang::as_string(rlang::get_expr(df)), value = value, envir = rlang::get_env(df))
      )
    )
  )
  value
}
