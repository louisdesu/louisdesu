#' Create custom theme for UNSW publications
#'
#' This function creates a custom theme for UNSW publications. It extends the base `theme_light()` and replaces specific theme elements with custom styles.
#'
#' @param base_size The base font size (default: 11).
#' @param base_family The base font family (default: "").
#' @return A custom theme for missing/removed data.
#' @importFrom ggplot2 theme theme_replace theme_light element_text margin unit
#' @examples
#' require(ggplot2)
#' ggplot(mtcars, aes(x = mpg, y = cyl)) + geom_point() + theme_UNSW()
#' @export
theme_UNSW <- function(base_size = 11, base_family = "") {
  # Add font
  fontpath <- sysfonts::font_paths()
  sysfonts::font_add(family = "Sommet", regular = "Sommet.otf", bold = "SommetBold.otf", bolditalic = "SommetBoldItalic.otf", italic = "SommetItalic.otf") # italic is buggy
  showtext::showtext_auto()
  
  # Custom theming
  ggplot2::theme_light() %+replace%
    ggplot2::theme(
      text = ggplot2::element_text(family = "Sommet"),
      axis.title = ggplot2::element_text(face = "bold"),
      strip.text.x = ggplot2::element_text(colour = "black", face = "bold"),
      strip.text.y = ggplot2::element_text(colour = "black", face = "bold", angle = 270),
      plot.title = ggplot2::element_text(colour = "black", face = "bold", size = 16, hjust = 0, margin = ggplot2::margin(0, 0, 5, 0)),
      plot.subtitle = ggplot2::element_text(colour = "black", face = "italic", size = 12, hjust = 0, margin = ggplot2::margin(0, 0, 10, 0)),
      plot.caption = ggplot2::element_text(colour = "black", face = "italic", size = 7, hjust = 1, margin = ggplot2::margin(3, 0, 10, 0)),
      legend.title = ggplot2::element_text(face = "bold", hjust = 0.02),
      legend.text = ggplot2::element_text(size = 10),
      strip.text.y.left = ggplot2::element_text(angle = 0),
      strip.placement = "outside",
      legend.key.size = ggplot2::unit(0.4, "cm")
    )
}