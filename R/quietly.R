#' Execute code quietly
#'
#' This function allows you to execute code while controlling the suppression of messages, warnings, and output.
#'
#' @param ... Code to be executed.
#' @param messages Logical indicating whether to suppress messages (default: FALSE).
#' @param warnings Logical indicating whether to suppress warnings (default: FALSE).
#' @param output Logical indicating whether to suppress both messages and warnings, and make the output invisible (default: FALSE).
#' @return The result of the code execution.
#' @examples
#' quietly(print("Hello World!"))
#' quietly(warning("This is a warning!"), warnings = TRUE)
#' quietly(message("This is a message!"), messages = TRUE)
#' @export
quietly <- function(..., messages = FALSE, warnings = FALSE, output = FALSE) {
  if (messages && warnings && !output) {
    invisible(...)
  } else if (messages && !warnings && output) {
    suppressMessages(invisible(...))
  } else if (messages && !warnings && !output) {
    suppressMessages(...)
  } else if (!messages && warnings && output) {
    suppressWarnings(invisible(...))
  } else if (!messages && warnings && !output) {
    suppressWarnings(...)
  } else if (!messages && !warnings && output) {
    suppressWarnings(suppressMessages(invisible(...)))
  } else if (!messages && !warnings && !output) {
    suppressWarnings(suppressMessages(...))
  }
}
