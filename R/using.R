#' Install and load packages
#'
#' This function installs and loads the specified packages. It checks if the packages are already installed, and if not, it installs them from source and then loads them.
#'
#' @param ... Character vectors specifying the packages to install and load.
#' @param load Logical indicating whether to load the packages after installation (default: TRUE).
#' @param lips Logical indicating whether to display installation messages (default: FALSE).
#' @param junk Logical indicating whether to install package dependencies (default: TRUE).
#' @importFrom pacman p_load
#' @importFrom utils install.packages
#' @examples
#' \dontrun{
#' using("tidyr", load = FALSE)
#' }
#' @export
using <- function(...,load=TRUE,lips=FALSE,junk=TRUE) {
  libs <- unlist(list(...))
  reqs <- quietly(unlist(lapply(libs,require,character.only=TRUE)))
  need <- libs[reqs==FALSE]
  
  # install missing packages
  if (length(need) > 0){
    if (length(need > 1)) {print(paste("Installing:",paste(need,collapse=", ")))}
    install.packages(need,type="source",quiet=lips,dependencies=junk)
    quietly(lapply(need,require,character.only=TRUE))
  }
  # load nominated packages
  if (load == FALSE) {} else {
    print(paste("Loading:",paste(libs,collapse=", ")))
    for (i in seq_along(libs)) {
      quietly(library(libs[i],character.only=TRUE))
    }
  }
}
