#' Save output quietly within a piping workflow
#'
#' This function saves the output of the preceding expression within a piping workflow to a CSV file named "temp.csv" without printing any messages.
#'
#' @name %S>%
#' @param value The value to save.
#' @param df A slang expression containing the variable name and environment.
#' @return The original value.
#' @importFrom readr write_csv
#' @importFrom rlang enquo
#' @examples
#' require(magrittr)
#' start <- rnorm(100)
#' finish <- function(x) mean(x)
#' start %S>% finish
#' @export
`%S>%` <- function(value, df) {
  df <- rlang::enquo(df)
  if (!is.data.frame(value)) {
    value <- data.frame(result = value)
  }
  suppressWarnings(
    suppressMessages(
      invisible(
        readr::write_csv(value, "temp.csv")
      )
    )
  )
  value
}
